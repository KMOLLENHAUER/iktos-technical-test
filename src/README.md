# Iktos technical test

## Build image
```bash

docker build -t iktos-app .
```
## Run image
```bash
docker run --rm -it -p 5000:5000 iktos-app
```
Default

```bash
docker run --rm -it -p 5000:5000    \
    -e APP_PORT=5555                \
    -e DB_URL=mongodb://<ip>:27017/ \
    iktos-app
```

## Get current schemes
```bash
curl -X GET http://localhost:5000/
```
return list of schemes already declared or empty list otherwise

## Post dataset
```bash
curl -X POST http://localhost:5000/<scheme_id> \
     -H "Content-Type: multipart/form-data"         \
     -F "file=@dataset.csv"
```
Return number of line inserted into the DB

## Get clean dataset
```bash
curl -X GET http://localhost:5000/<scheme_id>
```
Return all the clean lines of the dataset.
