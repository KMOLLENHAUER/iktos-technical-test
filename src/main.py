import logging
import getopt
import sys
import json
import pandas as pd

from dataset_checker import check_dataset

from  bson.objectid import ObjectId
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from flask import Flask, request
app = Flask(__name__)

LOG = logging.getLogger('app')
MONGO_CLI = None
DB = None

"""
Return all the present schemes
"""
@app.route("/", methods = ['GET'])
def get_schemes():
    LOG.debug("Get all schemes")
    # Retreive all the store schemes
    schemes_col = DB.scheme_collection
    schemes = schemes_col.find()

    res = []
    for s in schemes:
        res.append(s)

    # Return them
    return str(res), 200

"""
Append a new scheme
"""
@app.route("/", methods = ['POST'])
def post_scheme():
    LOG.debug("Post a new scheme")

    # Retreive the scheme from the request data
    scheme_def = request.get_json()

    # Insert it into the scheme collection
    schemes_col = DB.scheme_collection

    scheme_id = schemes_col.insert_one(scheme_def).inserted_id

    LOG.info('Scheme: {} \n Inserted at id: {}'.format(
                            scheme_def,
                            scheme_id)
            )
    return str(scheme_id), 200

"""
Append to <scheme_id> scheme the given dataset
"""
@app.route("/<scheme_id>", methods = ['POST'])
def post_dataset(scheme_id):
    LOG.debug("Post a new dataset in scheme {}".format(scheme_id))
    # Transform scheme_id to MongDB scheme id
    objId = ObjectId(scheme_id)
    # Get the scheme scheme_collection
    scheme_col = DB.scheme_collection

    # Retreive the scheme
    scheme = scheme_col.find_one({"_id": objId})

    if scheme == None:
        return 'Scheme not found', 500
    # Remove useless filed for the check
    del(scheme['_id'])
    LOG.debug('Retrieved scheme: {}'.format(str(scheme)))

    # Retrieve the dataset
    dataset = pd.read_csv(request.files['file'], sep='|')

    LOG.debug(dataset.head())

    # Pass dataset and scheme to the clear function
    dataset = check_dataset(dataset, scheme)

    # Get the dataset collection
    dataset_col = DB[scheme_id]

    LOG.debug(dataset.head())

    records = json.loads(dataset.T.to_json()).values()

    LOG.debug("Records to insert: {}".format(str(records)))

    dataset_col.insert_many(records)

    # Return number of line inserted
    return str(dataset.shape[0]), 200

"""
Return dataset(s) from the <sheme_id> scheme
"""
@app.route("/<scheme_id>", methods = ['GET'])
def get_datasets(scheme_id):
    LOG.debug("Get dataset for scheme {}".format(scheme_id))
    # Transform scheme_id to MongDB scheme id
    objId = ObjectId(scheme_id)
    # Get the scheme scheme_collection
    scheme_col = DB.scheme_collection

    # Retreive the scheme
    scheme = scheme_col.find_one({"_id": objId})

    if scheme == None:
        return 'Scheme not found', 500

    # Get the dataset collection
    dataset_col = DB[scheme_id]

    cursor = dataset_col.find()

    dataset = pd.DataFrame(list(cursor))

    #remove _id col
    del dataset['_id']

    return dataset.to_csv(sep='|'), 200

if __name__ == "__main__":
    # Set the default log file name
    log_file = "log.txt"
    # Set the default port
    port = 5000
    # Default MongoDB URI
    db_uri = "mongodb://localhost:27017/"

    try:
        opts, args = getopt.getopt(sys.argv[1:], "p:l:d", ["port=", "log_file=", "db="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-p", "--port"):
            port = arg
        elif opt in ("-l", "--log_file"):
            log_file = arg
        elif opt in ("-d", "--db"):
            db_uri = arg

    print('Using MongoDB URI: {}'.format(db_uri))
    print('App listening on port: {}'.format(port))

    logging.basicConfig(filename='log.txt',
                        level=logging.DEBUG,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%y-%m-%d %H:%M:%S')

    LOG.info('Using MongoDB URI: {}'.format(db_uri))
    MONGO_CLI = MongoClient(db_uri)
    # pymongo does not return state at connection, need to send command to
    # know if the current state is ok
    try:
        DB = MONGO_CLI.app_database
    except ConnectionFailure:
        LOG.error("Server not available")
        sys.exit(2)

    app.run(host='0.0.0.0', port=port)
