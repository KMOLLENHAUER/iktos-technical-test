import pandas as pd

"""
Clear the dataset
"""
def check_dataset(dataset :pd.DataFrame, scheme :dict={}) -> pd.DataFrame:
    # Convert the scheme
    scheme = convert_scheme(scheme)

    # Drop na lines
    dataset = dataset.dropna(axis='index')

    return dataset

"""
Convert scheme to pandas compliant scheme
"""
def convert_scheme(scheme:dict)->dict:
    pd_scheme = {}
    for f in scheme:
        if scheme[f] == 'int':
            pd_scheme[f] = 'int32'
        if scheme[f] == 'string':
            pd_scheme[f] = 'str'
        if scheme[f] == 'date':
            pd_scheme[f] = 'datetime64'
    return pd_scheme
