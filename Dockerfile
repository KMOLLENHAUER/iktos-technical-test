FROM python:3

ENV APP_PORT "5000"
ENV DB_URL "mongodb://localhost:27017/"

# Add sources
COPY . /app

# Install requirements
RUN pip install -r /app/requirement.txt

ENTRYPOINT python /app/src/main.py -p $APP_PORT --db $DB_URL
